package com.example.lotters.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.lotters.R;
import com.example.lotters.ViewModels.QuestionViewmodel;
import com.example.lotters.classes.Answer;
import com.example.lotters.classes.Question;
import com.example.lotters.classes.User;
import com.example.lotters.recyclerviews.AnswersRecyclerView;
import com.example.lotters.recyclerviews.QuestionsRecyclerView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;


public class AnswersForQuestionFragment extends Fragment implements AnswersRecyclerView.answerCardListener {

    private Button backButton;
    private QuestionViewmodel viewmodel;
    private String questionUID;
    private AnswersRecyclerView adapter;
    private ArrayList<Answer> answerCards = new ArrayList<>();
    private FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
    private FirebaseUser currentUser = firebaseAuth.getCurrentUser();
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference dbAnswers = db.collection("Answers");
    private CollectionReference dbQuestions = db.collection("Questions");
    private CollectionReference dbUsers = db.collection("Users");
    private ListenerRegistration listenerRegistration;
    private Integer fishseInPocket;
    private int scrollPosition;

    public AnswersForQuestionFragment() {
        // Required empty public constructor
    }

    public static AnswersForQuestionFragment newInstance(String questionUID, int scrollPosition) {
        AnswersForQuestionFragment fragment = new AnswersForQuestionFragment();
        Bundle args = new Bundle();
        args.putString("questionUID", questionUID);
        args.putInt("scrollPosition", scrollPosition);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            questionUID = getArguments().getString("questionUID");
            scrollPosition = getArguments().getInt("scrollPosition");
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_answers_for_question, container, false);

        backButton = view.findViewById(R.id.afqBackButton);

        viewmodel = new ViewModelProvider(getActivity()).get(QuestionViewmodel.class);

        RecyclerView recyclerView = view.findViewById(R.id.answersForQuestionRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new AnswersRecyclerView(getContext(), answerCards, this, true, currentUser.getUid());
        recyclerView.setAdapter(adapter);


        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeAnswersReturnToQuestion();
            }
        });

        return view;
    }


    @Override
    public void onStart() {
        super.onStart();


        if ( currentUser != null && questionUID != null){
            listenerRegistration = dbAnswers.whereEqualTo("questionID", questionUID).orderBy("date", Query.Direction.DESCENDING).limit(10)
                    .addSnapshotListener(new EventListener<QuerySnapshot>() {
                        @Override
                        public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                            if ( error != null ){
                                Log.d("listener", "error");
                                return;
                            }

                            List<Answer> databaseAnswers = new ArrayList<>();

                            for (QueryDocumentSnapshot document : value ){
                                if( document.exists() ){
                                    databaseAnswers.add(document.toObject(Answer.class));
                                }
                            }

                            updateRecyclerView(databaseAnswers);
                        }
                    });
        } else {
            Log.d("onactivitycreated: ", "questionID null");
        }
    }

    private void initializeAdapter(String questionUID) {
        dbAnswers.whereEqualTo("questionID", questionUID).orderBy("date", Query.Direction.DESCENDING)
                .get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()){

                            List<Answer> databaseAnswers = new ArrayList<>();

                            for ( QueryDocumentSnapshot documentSnapshot : task.getResult()){
                                if ( documentSnapshot.exists() ){
                                    databaseAnswers.add(documentSnapshot.toObject(Answer.class));
                                }
                            }

                            updateRecyclerView(databaseAnswers);

                        } else {

                        }
                    }
                });
    }


    private void updateRecyclerView(List<Answer> databaseAnswers) {

        answerCards.clear();
        answerCards.addAll(databaseAnswers);
        adapter.notifyDataSetChanged();

    }

    private void closeAnswersReturnToQuestion() {

        NavController navController;
        NavHostFragment navHostFragment;

        navHostFragment = (NavHostFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.navhost);
        navController = navHostFragment.getNavController();

        Bundle arguments = new Bundle();
        arguments.putInt("scrollPosition", scrollPosition);

        navController.navigate(R.id.navigateFromAnswerForQuestionToQuestions, arguments);

    }


    @Override
    public void onBuyBodyButtonPressed(int index) {

        Integer answerBodyCost = Integer.parseInt(answerCards.get(index).getFishesCost());
        Integer previewCost = Integer.parseInt(answerCards.get(index).getPreviewCost());
        String poster = answerCards.get(index).getUserID();

        dbUsers.document(currentUser.getUid()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull @NotNull Task<DocumentSnapshot> task) {
                if ( task.isSuccessful() ){

                    if ( task.getResult().exists() )  fishseInPocket = Integer.parseInt(task.getResult().get("fishes").toString());

                    if ( fishseInPocket != null && answerCards.get(index).getAnswerBodyBoughtBy().equals("none")){

                        if ( fishseInPocket >= ( answerBodyCost + previewCost ) ) {
                            adapter.updateBody(index);
                            adapter.notifyDataSetChanged();
                            dbAnswers.document(answerCards.get(index).getAnswerUID()).update("answerBodyBoughtBy", currentUser.getUid());
                            dbAnswers.document(answerCards.get(index).getAnswerUID()).update("previewBoughtBy", currentUser.getUid());
                            fishseInPocket -= ( answerBodyCost + previewCost );
                            dbUsers.document(currentUser.getUid()).update("fishes", fishseInPocket.toString());
                            Toast.makeText(getContext(), "Pocket is lighter. Fishes left: " + fishseInPocket, Toast.LENGTH_SHORT).show();
                            dbUsers.document(poster).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                @Override
                                public void onComplete(@NonNull @NotNull Task<DocumentSnapshot> task) {
                                    if (task.isSuccessful()) {
                                        Integer posterFishes = Integer.parseInt(task.getResult().get("fishes").toString());

                                        posterFishes += ( answerBodyCost + previewCost );
                                        dbUsers.document(poster).update("fishes", posterFishes.toString());
                                    } else {
                                        Log.d("afq: ", "read poster fishes error");
                                    }
                                }
                            });

                            dbUsers.document(answerCards.get(index).getUserID()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                @Override
                                public void onComplete(@NonNull @NotNull Task<DocumentSnapshot> task) {
                                    if ( task.isSuccessful() ){
                                        User user = task.getResult().toObject(User.class);
                                        int xp;
                                        xp = user.getXp();
                                        xp += 30;
                                        dbUsers.document(answerCards.get(index).getUserID()).update("xp", xp);

                                    }
                                }
                            });

                        } else {
                            Toast.makeText(getContext(), "Not enough fishes. You should go fishin'", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Toast.makeText(getContext(), "You already traded for this knowledge", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Log.d("buy button in afq: ", "error");
                }
            }
        });


    }

    @Override
    public void onBuyPreviewButtonPressed(int index) {

        Integer answerPreviewCost = Integer.parseInt(answerCards.get(index).getPreviewCost());
        String poster = answerCards.get(index).getUserID();

        dbUsers.document(currentUser.getUid()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull @NotNull Task<DocumentSnapshot> task) {
                if ( task.isSuccessful() ){

                    if ( task.getResult().exists() )  fishseInPocket = Integer.parseInt(task.getResult().get("fishes").toString());

                    if ( fishseInPocket != null && answerCards.get(index).getPreviewBoughtBy().equals("none")){

                        if ( fishseInPocket >=  answerPreviewCost) {
                            adapter.updatePreview(index);
                            adapter.notifyDataSetChanged();
                            dbAnswers.document(answerCards.get(index).getAnswerUID()).update("previewBoughtBy", currentUser.getUid());
                            fishseInPocket -= answerPreviewCost;
                            dbUsers.document(currentUser.getUid()).update("fishes", fishseInPocket.toString());
                            Toast.makeText(getContext(), "Pocket is lighter. Fishes left: " + fishseInPocket, Toast.LENGTH_SHORT).show();
                            dbUsers.document(poster).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                @Override
                                public void onComplete(@NonNull @NotNull Task<DocumentSnapshot> task) {
                                    if (task.isSuccessful()) {
                                        Integer posterFishes = Integer.parseInt(task.getResult().get("fishes").toString());

                                        posterFishes += answerPreviewCost;
                                        dbUsers.document(poster).update("fishes", posterFishes.toString());
                                    } else {
                                        Log.d("afq: ", "read poster fishes error");
                                    }
                                }
                            });

                            dbUsers.document(answerCards.get(index).getUserID()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                @Override
                                public void onComplete(@NonNull @NotNull Task<DocumentSnapshot> task) {
                                    if ( task.isSuccessful() ){
                                        User user = task.getResult().toObject(User.class);
                                        int xp;
                                        xp = user.getXp();
                                        xp += 15;
                                        dbUsers.document(answerCards.get(index).getUserID()).update("xp", xp);

                                    }
                                }
                            });

                        } else {
                            Toast.makeText(getContext(), "Not enough fishes. You should go fishin'", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getContext(), "You already traded for this knowledge", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Log.d("buy button in afq: ", "error");
                }
            }
        });

    }

    @Override
    public void onRemoveButtonPressed(int index) {

    }
}