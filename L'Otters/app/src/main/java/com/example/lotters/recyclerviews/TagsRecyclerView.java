package com.example.lotters.recyclerviews;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.lotters.R;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class TagsRecyclerView extends RecyclerView.Adapter<TagsRecyclerView.TagsViewHolder> {

    private List<String> data;
    private LayoutInflater inflater;

    public TagsRecyclerView(Context context, List<String> fetchedData){
        this.data = fetchedData;
        this.inflater = LayoutInflater.from(context);
    }

    @NonNull
    @NotNull
    @Override
    public TagsViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.tags_recycler_layout, parent, false);
        return new TagsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull TagsRecyclerView.TagsViewHolder holder, int position) {
        holder.tag.setText(data.get(position));
    }

    @Override
    public int getItemCount() {
        if ( data != null ) return data.size();
        return 0;
    }

    public class TagsViewHolder extends RecyclerView.ViewHolder {

        TextView tag;

        TagsViewHolder ( View view){
            super(view);

            tag = view.findViewById(R.id.tagLayoutText);

        }
    }

}
