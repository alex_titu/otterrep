package com.example.lotters.fragments;

import android.content.Context;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lotters.R;
import com.example.lotters.classes.Question;
import com.example.lotters.classes.User;
import com.example.lotters.recyclerviews.PreferencesRecyclerView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.slider.RangeSlider;
import com.google.android.material.slider.Slider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;


import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;


public class QuestionFormularFragment extends Fragment implements PreferencesRecyclerView.OnRemoveListener {


    private ImageButton returnToQuestions;
    private Button addPref, postQuestion;
    private TextView boyText, girlText, agePup, ageAdolescent, ageGrownup, ageSenior;
    private EditText status, preference, title, questionBody;
    private ArrayList<String> prefArray = new ArrayList<>();
    private PreferencesRecyclerView adapter;
    private Typeface boldTypeface = Typeface.defaultFromStyle(Typeface.BOLD);
    private Typeface normalTypeface = Typeface.defaultFromStyle(Typeface.NORMAL);
    private FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
    private FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference dbQuestions = db.collection("Questions");
    private CollectionReference dbUsers = db.collection("Users");
    private String gender, age;
    private Slider rangeSlider;
    private StorageReference storageReference = FirebaseStorage.getInstance().getReference();
    private StorageReference fileRef;
    private int questionBudget = 0;


    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }


    public QuestionFormularFragment() {
        // Required empty public constructor
    }


    public static QuestionFormularFragment newInstance() {
        QuestionFormularFragment fragment = new QuestionFormularFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_question_formular, container, false);

        returnToQuestions = view.findViewById(R.id.returntoQuestions);
        addPref = view.findViewById(R.id.qfAddPreferenceButton);
        agePup = view.findViewById(R.id.qfAgePup);
        ageAdolescent = view.findViewById(R.id.qfAgeAdolescnet);
        ageGrownup = view.findViewById(R.id.qfAgeGrownUp);
        ageSenior = view.findViewById(R.id.qfAgeSenior);
        boyText = view.findViewById(R.id.qfGenderBoy);
        girlText = view.findViewById(R.id.qfGenderGirl);
        status = view.findViewById(R.id.qfStatus);
        preference = view.findViewById(R.id.qfPreferenceInput);
        postQuestion = view.findViewById(R.id.qfPostQuestion);
        title = view.findViewById(R.id.qfTitle);
        questionBody = view.findViewById(R.id.qfQuestionBody);
        rangeSlider = view.findViewById(R.id.qfPriceRange);

        fileRef = storageReference.child("Users/"+firebaseUser.getUid()+"/profile.jpg");

        rangeSlider.setValueFrom(0);
        rangeSlider.setValueTo(500);
        rangeSlider.addOnChangeListener(new Slider.OnChangeListener() {
            @Override
            public void onValueChange(@NonNull Slider slider, float value, boolean fromUser) {
                questionBudget = (int) value;
            }
        });

        RecyclerView recyclerView = view.findViewById(R.id.qfPreferencesRecyclerView);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        adapter = new PreferencesRecyclerView(getContext(), prefArray, this);
        recyclerView.setAdapter(adapter);

        boyText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( boyText.getTypeface().equals(boldTypeface) ) {
                    boyText.setTypeface(normalTypeface);
                    gender = "unknown";
                } else {
                    boyText.setTypeface(boldTypeface);
                    gender = "boy";
                    girlText.setTypeface(normalTypeface);
                }
            }
        });

        girlText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( girlText.getTypeface().equals(boldTypeface) ) {
                    girlText.setTypeface(normalTypeface);
                    gender = "unknown";
                } else{
                    girlText.setTypeface(boldTypeface);
                    gender = "girl";
                    boyText.setTypeface(normalTypeface);
                }

            }
        });

        agePup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( agePup.getTypeface().equals(boldTypeface)){
                    agePup.setTypeface(normalTypeface);
                    age = "unknown";
                } else {
                    agePup.setTypeface(boldTypeface);
                    ageAdolescent.setTypeface(normalTypeface);
                    age = "pup";
                    ageGrownup.setTypeface(normalTypeface);
                    ageSenior.setTypeface(normalTypeface);
                }
            }
        });

        ageAdolescent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( ageAdolescent.getTypeface().equals(boldTypeface)){
                    ageAdolescent.setTypeface(normalTypeface);
                    age = "unknown";
                } else {
                    ageAdolescent.setTypeface(boldTypeface);
                    agePup.setTypeface(normalTypeface);
                    ageGrownup.setTypeface(normalTypeface);
                    age = "adolescent";
                    ageSenior.setTypeface(normalTypeface);
                }
            }
        });

        ageGrownup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( ageGrownup.getTypeface().equals(boldTypeface)){
                    ageGrownup.setTypeface(normalTypeface);
                    age = "unknown";
                } else {
                    ageGrownup.setTypeface(boldTypeface);
                    ageAdolescent.setTypeface(normalTypeface);
                    agePup.setTypeface(normalTypeface);
                    age = "grownup";
                    ageSenior.setTypeface(normalTypeface);
                }
            }
        });

        ageSenior.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( ageSenior.getTypeface().equals(boldTypeface)){
                    ageSenior.setTypeface(normalTypeface);
                    age = "unknown";
                } else {
                    ageSenior.setTypeface(boldTypeface);
                    ageAdolescent.setTypeface(normalTypeface);
                    age = "senior";
                    ageGrownup.setTypeface(normalTypeface);
                    agePup.setTypeface(normalTypeface);
                }
            }
        });

        returnToQuestions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(view).navigate(R.id.navigateFromQuestionFormularToQuestions);
            }
        });

        addPref.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( !preference.getText().toString().equals("")) {
                    addPrefernece(preference.getText().toString());
                    preference.setText("");
                }
                else Toast.makeText(getActivity(), "Please type a preference", Toast.LENGTH_SHORT).show();
            }
        });

        postQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Question question = new Question();

                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
                Date date = new Date();
                String dateTime = dateFormat.format(date);


                if(!isNetworkAvailable())  Toast.makeText(getContext(), "No internet connection", Toast.LENGTH_SHORT).show();
                else {
                    if ( !status.getText().toString().equals("") && !title.getText().toString().equals("") && !questionBody.getText().toString().equals("") && !prefArray.isEmpty() ){
                        if ( firebaseUser != null ) {

                            question.setUserID(firebaseUser.getUid());
                            question.setTitle(title.getText().toString());
                            question.setAge(age);
                            question.setDescription(questionBody.getText().toString());
                            question.setGender(gender);
                            question.setStatus(status.getText().toString());
                            question.setTags(prefArray);
                            question.setDate(dateTime);
                            question.setQuestionID(UUID.randomUUID().toString());
                            question.setBudget(questionBudget);

                            dbUsers.document(firebaseUser.getUid()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<DocumentSnapshot> task) {

                                    if (task.isSuccessful()) {
                                        DocumentSnapshot documentSnapshot = task.getResult();

                                        if (documentSnapshot.exists()) {
                                            User user = documentSnapshot.toObject(User.class);

                                            question.setUsername(user.getUsername());
                                            postQuestionToDatabase(question);

                                        }
                                    }
                                }
                            });

                            Navigation.findNavController(view).navigate(R.id.navigateFromQuestionFormularToQuestions);
                        }

                    } else {
                        Toast.makeText(getActivity(), "Title, Description and at least one Preference are mandatory  ",
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        return view;
    }

    private void postQuestionToDatabase(Question question) {

        dbQuestions.document(question.getQuestionID()).set(question).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if( task.isSuccessful() ){
                    Log.d("question upload", "success");
                } else {
                    Log.d("question upload", "failure");
                }
            }
        });

    }

    private void addPrefernece(String pref) {
        prefArray.add(pref);
        adapter.notifyItemInserted(0);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onRemove(int index) {

    }
}