package com.example.lotters.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.lotters.R;
import com.example.lotters.activities.registerActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.SignInMethodQueryResult;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;

import org.jetbrains.annotations.NotNull;

public class ChangeEmailFragment extends Fragment {

    private FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
    private FirebaseUser currentUser = firebaseAuth.getCurrentUser();
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference dbUsers = db.collection("Users");
    private String email,password;
    private EditText currentEmail,currentPassword, newEmail, confNewEmail;
    private Button confirmChangeEmail;
    private ImageView backToSettings;

    public ChangeEmailFragment() {
        // Required empty public constructor
    }

    public static ChangeEmailFragment newInstance() {
        ChangeEmailFragment fragment = new ChangeEmailFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public boolean isEmailValid(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_change_email, container, false);
        currentEmail = view.findViewById(R.id.current_email);
        currentPassword = view.findViewById(R.id.current_password);
        newEmail = view.findViewById(R.id.new_email);
        confNewEmail = view.findViewById(R.id.conf_new_email);
        confirmChangeEmail = view.findViewById(R.id.confirm_btn_email);
        backToSettings = view.findViewById(R.id.cef_backbutton);

        confirmChangeEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (currentEmail.getText().toString().matches("") ||
                        currentPassword.getText().toString().equals("") || newEmail.getText().toString().equals("")
                        || confNewEmail.getText().toString().equals(""))
                    Toast.makeText(getContext(), "All fields are mandatory", Toast.LENGTH_SHORT).show();

                else if (!isEmailValid(newEmail.getText().toString()))
                    Toast.makeText(getContext(), "New E-mail Invalid", Toast.LENGTH_SHORT).show();

                else {
                    email = currentEmail.getText().toString();
                    password = currentPassword.getText().toString();
                    AuthCredential credential = EmailAuthProvider.getCredential(email, password);

                    firebaseAuth.fetchSignInMethodsForEmail(newEmail.getText().toString())
                            .addOnCompleteListener(new OnCompleteListener<SignInMethodQueryResult>() {
                                @Override
                                public void onComplete(@NonNull Task<SignInMethodQueryResult> task) {
                                    boolean isNewUser = task.getResult().getSignInMethods().isEmpty();

                                    if (isNewUser) {
                                        Log.d("Change Email Fragment", "Is New Email!");

                                        if (!newEmail.getText().toString().equals(confNewEmail.getText().toString())) {
                                            Toast.makeText(getContext(), "Emails don't match", Toast.LENGTH_SHORT).show();

                                        } else {

                                            currentUser.reauthenticate(credential).addOnSuccessListener(new OnSuccessListener<Void>() {
                                                @Override
                                                public void onSuccess(Void unused) {

                                                    currentUser.updateEmail(newEmail.getText().toString());
                                                    dbUsers.document(currentUser.getUid()).update("email", newEmail.getText().toString());
                                                    Navigation.findNavController(view).navigate(R.id.navigateFromChangeEmailFragmentToSettingsFragment);

                                                }
                                            }).addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(@NonNull @NotNull Exception e) {
                                                    Toast.makeText(getContext(), "Wrong current E-mail or Password", Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                        }

                                    } else {
                                        Log.d("Change Email Fragment", "Is Existing Email!");
                                        Toast.makeText(getContext(), "Email already used",
                                                Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                }
            }
        });

        backToSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigation.findNavController(view).navigate(R.id.navigateFromChangeEmailFragmentToSettingsFragment);
            }
        });

        return view;
    }
}