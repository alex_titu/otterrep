package com.example.lotters.recyclerviews;

        import android.animation.Animator;
        import android.animation.ObjectAnimator;
        import android.content.Context;
        import android.net.Uri;
        import android.util.Log;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.AdapterView;
        import android.widget.Button;
        import android.widget.Filter;
        import android.widget.Filterable;
        import android.widget.ImageButton;
        import android.widget.ImageView;
        import android.widget.TextView;
        import android.widget.Toast;

        import androidx.annotation.NonNull;
        import androidx.recyclerview.widget.RecyclerView;

        import com.example.lotters.R;
        import com.example.lotters.classes.Answer;
        import com.example.lotters.classes.Question;
        import com.google.android.gms.tasks.OnCompleteListener;
        import com.google.android.gms.tasks.Task;
        import com.google.firebase.storage.FirebaseStorage;
        import com.google.firebase.storage.StorageReference;
        import com.squareup.picasso.Picasso;

        import org.jetbrains.annotations.NotNull;
        import org.w3c.dom.Text;

        import java.util.ArrayList;
        import java.util.List;

public class AnswersRecyclerView extends RecyclerView.Adapter<AnswersRecyclerView.AnswersViewHolder> {

    private List<Answer> data;
    private LayoutInflater layoutInflater;
    private answerCardListener listener;
    private Boolean visibility;
    private String currentUser;
    private StorageReference storageReference = FirebaseStorage.getInstance().getReference();
    private StorageReference picRef;

    public AnswersRecyclerView(Context context, List<Answer> fetchedData, answerCardListener listener, Boolean visibility, String currentUser ){
        this.layoutInflater = LayoutInflater.from(context);
        this.data = fetchedData;
        this.listener = listener;
        this.visibility = visibility;
        this.currentUser = currentUser;
    }

    @NonNull
    @Override
    public AnswersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.answerrow_layout, parent, false);
        return new AnswersViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AnswersViewHolder holder, int position) {
        String username, title, preview, answerBody, fishesCost, previewCost, rank;
        Integer budget;

        picRef = storageReference.child("Users/"+data.get(position).getUserID()+"/profile.jpg");
        if ( picRef != null )
            picRef.getDownloadUrl().addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull @NotNull Task<Uri> task) {
                    if ( task.isSuccessful()){
                        Picasso.get().load(task.getResult()).into(holder.profilePic);
                    }
                }
            });

        username = data.get(position).getUsername();
        title = data.get(position).getTitle();
        preview = data.get(position).getPreview();
        answerBody = data.get(position).getAnswerBody();
        fishesCost = data.get(position).getFishesCost();
        previewCost = data.get(position).getPreviewCost();
        rank = data.get(position).getUserRank();
        budget = data.get(position).getBudget();

        holder.budget.setText(String.valueOf(budget));
        holder.rank.setText(rank);
        holder.username.setText(username);
        holder.title.setText(title);
        holder.preview.setText(preview);
        holder.answerBody.setText(answerBody);
        holder.fishesCost.setText(fishesCost);

        if ( previewCost != null ) holder.previewCost.setText(previewCost);

        if ( !visibility ){
            if ( data.get(position).getUserID().equals(currentUser) ) {
                holder.preview.setVisibility(View.VISIBLE);
                holder.answerBody.setVisibility(View.VISIBLE);
            } else {

            }
        }

        if ( !visibility ) {
            if ( data.get(position).getUserID().equals(currentUser) ) holder.removeButton.setVisibility(View.VISIBLE);
             else holder.removeButton.setVisibility(View.INVISIBLE);
        }

        if ( data.get(position).getUserID().equals(currentUser) ) {

            holder.preview.setVisibility(View.VISIBLE);
            holder.answerBody.setVisibility(View.VISIBLE);

        } else if ( data.get(position).getAnswerBodyBoughtBy() != null && data.get(position).getAnswerBodyBoughtBy().equals(currentUser) ) {
                holder.answerBody.setVisibility(View.VISIBLE);
            if ( data.get(position).getPreviewBoughtBy() != null && data.get(position).getPreviewBoughtBy().equals(currentUser)) {
                holder.preview.setVisibility(View.VISIBLE);
            } else {
                holder.preview.setVisibility(View.INVISIBLE);
            }
        } else {
            holder.answerBody.setVisibility(View.INVISIBLE);
            if ( data.get(position).getPreviewBoughtBy() != null && data.get(position).getPreviewBoughtBy().equals(currentUser)) {
                holder.preview.setVisibility(View.VISIBLE);
            } else {
                holder.preview.setVisibility(View.INVISIBLE);
            }
        }

        holder.removeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onRemoveButtonPressed(position);
                data.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, data.size());
            }
        });

        holder.showMoreshowLess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sizeAnswerBody(holder.answerBody, holder.showMoreshowLess);
            }
        });

        holder.answerBody.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sizeAnswerBody(holder.answerBody, holder.showMoreshowLess);
            }
        });

        holder.bodyBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onBuyBodyButtonPressed(position);
            }
        });

        holder.previewBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onBuyPreviewButtonPressed(position);
            }
        });


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void updatePreview(int position){
        data.get(position).setPreviewBoughtBy(currentUser);
    }

    public void updateBody(int position){
        data.get(position).setAnswerBodyBoughtBy(currentUser);
        data.get(position).setPreviewBoughtBy(currentUser);
    }

    private void sizeAnswerBody(TextView textView , TextView showMoreLess){
        if (textView.getMaxLines() <= 2) {
            textView.setMaxLines(Integer.MAX_VALUE);
            showMoreLess.setText("show less");
        }
            else {
                textView.setMaxLines(2);
                showMoreLess.setText("show more");
        }
    }


    public class AnswersViewHolder extends RecyclerView.ViewHolder {

        TextView username, title, preview, answerBody, fishesCost, previewCost, rank, showMoreshowLess, budget;
        Button removeButton;
        ImageView profilePic, moneyPreview, moneyBody;
        ImageButton previewBuy, bodyBuy;

        AnswersViewHolder(View itemView) {
            super(itemView);

            username = itemView.findViewById(R.id.aUsername);
            title = itemView.findViewById(R.id.aTitle);
            preview = itemView.findViewById(R.id.aPreview);
            answerBody = itemView.findViewById(R.id.aAnswerBody);
            fishesCost = itemView.findViewById(R.id.aFishesCost);
            previewCost = itemView.findViewById(R.id.aPreviewCost);
            removeButton = itemView.findViewById(R.id.aRemoveButton);
            rank = itemView.findViewById(R.id.aUserRank);
            showMoreshowLess = itemView.findViewById(R.id.aShowMoreShowLess);
            budget = itemView.findViewById(R.id.aBudget);
            profilePic = itemView.findViewById(R.id.aProfilePicture);
            previewBuy = itemView.findViewById(R.id.aBuyPreviewButtonImg);
            bodyBuy = itemView.findViewById(R.id.aBodyBuyImg);
            moneyPreview = itemView.findViewById(R.id.aBuyPreviewMoney);
            moneyBody = itemView.findViewById(R.id.aBuyBodyMoney);

            answerBody.setMaxLines(2);
            showMoreshowLess.setVisibility(View.INVISIBLE);

            if ( !visibility ) {
                bodyBuy.setVisibility(View.INVISIBLE);
                moneyPreview.setVisibility(View.INVISIBLE);
                previewBuy.setVisibility(View.INVISIBLE);
                moneyBody.setVisibility(View.INVISIBLE);
            }

        }

    }

    public interface answerCardListener {
        void onBuyBodyButtonPressed(int index);
        void onBuyPreviewButtonPressed(int index);
        void onRemoveButtonPressed(int index);
    }
}
