package com.example.lotters.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lotters.R;
import com.example.lotters.classes.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.SignInMethodQueryResult;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.Calendar;

public class registerActivity extends AppCompatActivity {

    private EditText usernameRegister, passwordRegister, emailRegister, passwordCheck;
    private boolean emailNew;
    private Button registerButton;
    private FirebaseAuth mAuth;
    private FirebaseUser currentUser;
    private static final String TAG = "MyActivity";
    private TextView birthdateRegister;
    private DatePickerDialog.OnDateSetListener regDateSetListener;
    private ImageView returnToLogin;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference dbUsers = db.collection("Users");



    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }



    public boolean isEmailValid(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        FirebaseFirestore firebase = FirebaseFirestore.getInstance();

        usernameRegister = findViewById(R.id.nume);
        passwordRegister = findViewById(R.id.registerPassword);
        passwordCheck = findViewById(R.id.verifyPassword);
        emailRegister = findViewById(R.id.email);
        registerButton = findViewById(R.id.registerToFirebase);
        birthdateRegister = findViewById(R.id.reg_birthdate);
        returnToLogin = findViewById(R.id.reg_backbutton);

        mAuth = FirebaseAuth.getInstance();


        returnToLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), loginActivity.class);
                startActivity(intent);
                onBackPressed();
            }
        });

        birthdateRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        registerActivity.this, android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        regDateSetListener, year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        regDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                month=month+1;
                Log.d(TAG,"regDateSet: dd/mm/yyyy: " + dayOfMonth + "/" + month + "/" + year);

                String birthDate = dayOfMonth + "/" + month + "/" + year;
                birthdateRegister.setText(birthDate);
            }
        };


        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email, password, username, birthdate, fishes, crabs, onlineStatus, questions, passwordRetype;
                email = emailRegister.getText().toString();
                password = passwordRegister.getText().toString();
                passwordRetype = passwordCheck.getText().toString();
                username = usernameRegister.getText().toString();
                birthdate = birthdateRegister.getText().toString();
                fishes = "100";
                crabs = "0";
                onlineStatus = "online";


                if(!isNetworkAvailable()) Toast.makeText(getApplicationContext(), "No internet connection", Toast.LENGTH_SHORT).show();

                else if (username.matches("") || email.matches("") || password.matches("")
                || passwordRetype.matches("") || birthdate.matches("") )
                    Toast.makeText(getApplicationContext(), "All fields must be completed", Toast.LENGTH_SHORT).show();

                else if (!isEmailValid(email))
                    Toast.makeText(getApplicationContext(), "Your E-mail is Invalid", Toast.LENGTH_SHORT).show();

                else {

                    mAuth.fetchSignInMethodsForEmail(email)
                            .addOnCompleteListener(new OnCompleteListener<SignInMethodQueryResult>() {
                                @Override
                                public void onComplete(@NonNull Task<SignInMethodQueryResult> task) {
                                    boolean isNewUser = task.getResult().getSignInMethods().isEmpty();
                                    emailNew = isNewUser;
                                    if (isNewUser) {
                                        Log.d(TAG, "Is New User!");
                                    } else {
                                        Log.d(TAG, "Is Old User!");
                                    }
                                }
                            });


                    if (password.length() < 6)
                        Toast.makeText(getApplicationContext(), "Please make your password more than 5 characters", Toast.LENGTH_SHORT).show();

                    else if (!password.equals(passwordRetype))
                        Toast.makeText(getApplicationContext(), "Make sure you entered the password correctly both times", Toast.LENGTH_SHORT).show();


                    else {
                        mAuth.createUserWithEmailAndPassword(email, password)
                                .addOnCompleteListener(registerActivity.this, new OnCompleteListener<AuthResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                        if (task.isSuccessful()) {
                                            // Sign in success, update UI with the signed-in user's information
                                            Log.d(TAG, "createUserWithEmail:success");
                                            FirebaseUser user = mAuth.getCurrentUser();
                                            Toast.makeText(getApplicationContext(), "You are now registered", Toast.LENGTH_SHORT).show();

                                            updateUI(user);

                                        User databaseUser = new User(email, username, birthdate, fishes, crabs, onlineStatus, "Otter Squire", 0);

                                            dbUsers.document(user.getUid()).set(databaseUser).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    if (task.isSuccessful()) {
                                                        Log.d("firestore data register", user.getEmail() + " got data uploaded ");
                                                    } else {
                                                        Log.d("firestore data register", user.getEmail() + " had a problem ");
                                                    }
                                                }
                                            });

                                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                            startActivity(intent);

                                        } else {
                                            // If sign in fails, display a message to the user.
                                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                                            Toast.makeText(registerActivity.this, "Authentication failed.",
                                                    Toast.LENGTH_SHORT).show();
                                            updateUI(null);
                                            if (!emailNew) {
                                                Toast.makeText(registerActivity.this, "Email already used",
                                                        Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    }
                                });
                    }
                }
            }
        });

    }

    private void updateUI(FirebaseUser user) {
        currentUser = user;
    }

}