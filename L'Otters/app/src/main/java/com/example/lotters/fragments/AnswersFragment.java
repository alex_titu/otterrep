package com.example.lotters.fragments;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.lotters.R;
import com.example.lotters.ViewModels.QuestionViewmodel;
import com.example.lotters.classes.Answer;
import com.example.lotters.classes.Question;
import com.example.lotters.recyclerviews.AnswersRecyclerView;
import com.example.lotters.recyclerviews.QuestionsRecyclerView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;


import java.util.ArrayList;
import java.util.List;

public class AnswersFragment extends Fragment implements QuestionsRecyclerView.onRemoveQuestionCardListener{

    private QuestionsRecyclerView adapter;
    private ArrayList<Question> questionCards = new ArrayList<>();
    private FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
    private FirebaseUser currentUser = firebaseAuth.getCurrentUser();
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CollectionReference dbUsers = db.collection("Users");
    private CollectionReference dbQuestions = db.collection("Questions");
    private ListenerRegistration listenerRegistration;
    private int position;
    private QuestionViewmodel viewmodel;
    private RecyclerView recyclerView;
    private Boolean performSearch = false;


    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }


    public AnswersFragment() {
        // Required empty public constructor
    }

    public static AnswersFragment newInstance(String posYtoScrollTo) {
        AnswersFragment fragment = new AnswersFragment();
        Bundle args = new Bundle();
        args.putString("posYtoScrollTo", posYtoScrollTo);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            position = getArguments().getInt("posYtoScrollTo");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_answers, container, false);

        recyclerView = view.findViewById(R.id.aRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new QuestionsRecyclerView(getContext(), questionCards, this, false);
        recyclerView.setAdapter(adapter);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if ( !recyclerView.canScrollVertically(1) && !performSearch ){
                    refreshQuestions();
                }

            }
        });

        viewmodel = new ViewModelProvider(getActivity()).get(QuestionViewmodel.class);

        return view;
    }

    private void refreshQuestions() {

        int size = questionCards.size();

        if ( size-1 >= 0 ){
            String lastQuestionDate = questionCards.get(size-1).getDate();

            dbQuestions.whereLessThan("date", lastQuestionDate).orderBy("date", Query.Direction.DESCENDING).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    if ( task.isSuccessful() ){
                        List<Question> databaseQuestions = new ArrayList<>();

                        for ( QueryDocumentSnapshot doc : task.getResult()){
                            if (doc.exists()) {
                                databaseQuestions.add(doc.toObject(Question.class));
                            }
                        }

                        addToQuestionList(databaseQuestions);
                    }
                }
            });
        }

    }

    private void addToQuestionList(List<Question> list){
        questionCards.addAll(list);
        adapter.notifyDataSetChanged();
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        viewmodel.getSearchQuery().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if ( !s.equals("") ) {
                    dbQuestions.orderBy("date", Query.Direction.DESCENDING).limit(10).get()
                            .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                    if (task.isSuccessful()) {
                                        List<Question> filteredQuestions = new ArrayList<>();
                                        Question question = new Question();
                                        for (QueryDocumentSnapshot doc : task.getResult()) {
                                            question = doc.toObject(Question.class);

                                            for (String tag : question.getTags()) {
                                                if (tag.contains(s)) {
                                                    filteredQuestions.add(question);
                                                    break;
                                                }
                                            }
                                        }

                                        updateRecyclerView(filteredQuestions);
                                    }
                                }
                            });
                } else {
                    initializeQuestionCards();
                }
            }
        });

        viewmodel.getPerformSearch().observe(getViewLifecycleOwner(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                performSearch = aBoolean;
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();

        if ( currentUser != null ){

            listenerRegistration = dbQuestions.orderBy("date", Query.Direction.DESCENDING).limit(10)
                    .addSnapshotListener(new EventListener<QuerySnapshot>() {
                        @Override
                        public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                            if ( error != null ){
                                Log.d("listener", "error");
                                return;
                            }

                            List<Question> databaseQuestions = new ArrayList<>();

                            for (QueryDocumentSnapshot document : value ){
                                if( document.exists() ){
                                    databaseQuestions.add(document.toObject(Question.class));
                                }
                            }

                            updateRecyclerView(databaseQuestions);
                        }
                    });
        }
    }

    private void initializeQuestionCards() {

        if ( currentUser != null ) {

            dbQuestions.orderBy("date", Query.Direction.DESCENDING).limit(10).get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            List<Question> databaseQuestions = new ArrayList<>();

                            for (QueryDocumentSnapshot documentSnapshot : task.getResult()){
                                if ( documentSnapshot.exists()) databaseQuestions.add(documentSnapshot.toObject(Question.class));
                            }

                            updateRecyclerView(databaseQuestions);
                        }
                    });
        }
    }

    private void updateRecyclerView(List<Question> databaseQuestions) {

        questionCards.clear();
        questionCards.addAll(databaseQuestions);
        adapter.notifyDataSetChanged();
        recyclerView.scrollToPosition(position);

    }

    @Override
    public void onRemoveQuestion(int index) {

    }

    @Override
    public void onAnswerButtonPressed(int index) {

        if(!isNetworkAvailable()) Toast.makeText(getContext(), "No internet connection", Toast.LENGTH_SHORT).show();

        else {
            NavController navController;
            NavHostFragment navHostFragment;

            navHostFragment = (NavHostFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.navhost);
            navController = navHostFragment.getNavController();



        Bundle arguments = new Bundle();
        arguments.putString("questionUID", questionCards.get(index).getQuestionID());
        arguments.putInt("posYsaved", index);

        navController.navigate(R.id.navigateFromAnswersFragmentToListFragment, arguments);

        }
    }
}